;; Packages to install:
;; company
;; company-iron
;; iron-mode
;; highlight-indent-guides
;; flycheck-irony

;; Disable things
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; Melpa
(require 'package)
(add-to-list 'package-archives
  '("melpa-stable" . "https://stable.melpa.org/packages/"))

(package-initialize)

;; Enable ido mode
(ido-mode 1)

;; Ident
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default c-basic-offset 4)
(setq indent-line-function 'insert-tab)

;; Config paren mode
(show-paren-mode 1)
(setq show-paren-style 'parenthesis)

;; Disable autosave and backups
(setq make-backup-files nil)
(setq auto-save-default nil)

;; Line numbers
(global-linum-mode t)
;; Add theme path
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
;; Load themes
(load-theme 'dracula t)

;; Plugin configs
;; Ident guide
(setq highlight-indent-guides-method 'character)

;; Enable plugins
;; Enable company-mode
(global-company-mode t)
;; Ident guidelines
(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
;; Irony (c++ auto-complete)
(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))

;;(add-hook 'c++-mode-hook 'irony-mode)
;;(add-hook 'c-mode-hook 'irony-mode)
;;(add-hook 'objc-mode-hook 'irony-mode)

(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

;; Irony flycheck
(eval-after-load 'flycheck
'(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))
(global-flycheck-mode t)

(if (display-graphic-p) 
    (enable-theme 'dracula) 
  (disable-theme 'dracula))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (flycheck-irony company-irony irony highlight-indent-guides))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'downcase-region 'disabled nil)
